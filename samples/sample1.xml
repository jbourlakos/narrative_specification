<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xml>
<narrative 
	xmlns="http://gav.uop.gr/2017/narrative"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://gav.uop.gr/2017/narrative ../schema/narrative.xsd">
	<meta>
		<title>Names-Animals-Myths</title>
		<goals>Discover the relationships among names and wild animals throughout mythology.</goals>
		<language>en</language>
		<author>
			<person>
				<firstName>Angela</firstName>
				<lastName>Antoniou</lastName>
			</person>
			<email>angelant@uop.gr</email>
			<affiliation>Department of Informatics and Telecommunications, University of Peloponnese</affiliation>
		</author>
		<venue>Archaeological Museum of Tripoli, Tripoli, Arkadia, Greece</venue>
	</meta>
	<trail>
		<segment>
			<title>Introduction</title>
			<optional>false</optional>
			<duration>PT5M</duration>
			<narration>
				<part>
					<text>The truth is that names are a part of every culture and that they are of enormous importance both to the people who receive names and to the societies that given them. </text>
				</part>
				<part>
					<text>You can find naming ceremonies in most cultures and if interested you can access this link at any point for further information, see this Wikipedia page. Names have special meanings and are often connected to myths and legends. Ancient Greece is no different.</text>
					<media>
						<uri>https://en.wikipedia.org/wiki/Naming_ceremony</uri>
						<handler>web</handler>
					</media>
				</part>
				<part>
					<text>Follow us around the museum to see how names, wild animals and myths are all connected.</text>
				</part>
			</narration>
		</segment>
		<segment>
			<title>Anastasia Demestiha</title>
			<optional>true</optional>
			<constraints>
				<domain>
					<name>Physical Environment</name>
					<constraint>
						<property>Weather</property>
						<predicate>Is not</predicate>
						<value>Rainy</value>
					</constraint>
					<constraint>
						<property>Weather</property>
						<predicate>Is not</predicate>
						<value>Snowy</value>
					</constraint>
				</domain>
			</constraints>
			<duration>PT8M</duration>
			<exhibit>
				<id>...</id>
				<name>Anastasia Demestiha &amp; Museum Building</name>
				<location>Outside the venue, at the garden</location>
			</exhibit>
			<movement>false</movement>
			<narration>
				<part>
					<text>Anastasia Demestiha is the founder of the museum building.</text>
				</part>
				<part>
					<text>The history of Anastasia Demestiha.</text>
				</part>
				<part>
					<text>The history of the museum building.</text>
				</part>
			</narration>
		</segment>
		<segment>
			<title>Proceed to &quot;Hail Arcadia&quot; inscription</title>
			<optional>true</optional>
			<duration>PT1m</duration>
			<movement>true</movement>
		</segment>
		<segment>
			<title>&quot;Hail Arcadia&quot; inscription</title>
			<constraints>
				<domain>
					<name>Physical Environment</name>
					<constraint>
						<property>Weather</property>
						<predicate>Is not</predicate>
						<value>Rainy</value>
					</constraint>
					<constraint>
						<property>Weather</property>
						<predicate>Is not</predicate>
						<value>Snowy</value>
					</constraint>
				</domain>
			</constraints>
			<optional>false</optional>
			<duration>PT10M</duration>
			<exhibit>
				<id>...</id>
				<location>Room 7, 1st floor</location>
				<name>Museum Item 3084: &quot;Hail Arcadia&quot; tombstone</name>
			</exhibit>
			<narration>
				<part>
					<text>This inscription is at the museum entrance.</text>
				</part>
				<part>
					<text>How the entire area carries the name &quot;Arcadia&quot; and the myth about Arcadia and star constellations.</text>
				</part>
				<part>
					<text>The myth about Diana and Callisto.</text>
				</part>
				<part>
					<text>On your screen you can see a famous painting by Titian, housed in the National Gallery in London, picturing the scene in which Artemis discovers Callisto&apos;s pregnancy. Imagine how she must have felt. Titian beautifully shows her emotions. This particular painting has been used in educational programs of the National Gallery in London to show how myths and art can be relevant to today&apos;s students.</text>
					<media type="ReferredMedia">
						<handler>image</handler>
						<uri>https://www.nationalgallery.org.uk/server.iip?FIF=/fronts/N-6616-00-000027-WZ-PYR.tif&amp;CNT=1&amp;HEI=371&amp;QLT=85&amp;CVT=jpeg</uri>
					</media>
					<media type="ReferredMedia">
						<handler>web</handler>
						<uri>https://www.nationalgallery.org.uk/paintings/titian-diana-and-callisto</uri>
					</media>
				</part>
				<part>
					<text>You can also access a very interesting video on the issue of art, myths and hidden pregnancies, as a part of an educational curriculum for teenagers.</text>
					<media>
						<handler>web</handler>
						<uri>https://www.nationalgallery.org.uk/learning/teachers-and-schools/diana-and-callisto-schools-project</uri>
					</media>
				</part>
			</narration>
		</segment>
		<segment>
			<title>Move towards the museum</title>
			<optional>false</optional>
			<duration>PT1M</duration>
			<movement>true</movement>
		</segment>
		<segment>
			<title>Move to Room 7, 1st floor</title>
			<optional>false</optional>
			<duration>PT2M</duration>
			<movement>true</movement>
		</segment>
		<segment>
			<title>Statue of Demeter</title>
			<optional>false</optional>
			<duration>PT5M</duration>
			<exhibit>
				<id>...</id>
				<location>Room 7, 1st floor</location>
				<name>Museum Item: 2965 Statue of Demeter</name>
			</exhibit>
			<movement>false</movement>
			<narration>
				<part>
					<text>Myth about Demeter, Persephone and Hades.</text>
				</part>
				<part>
					<text>Worship of Demeter and Kore in Arcadia and relevant archaeological findings.</text>
					<media type="ReferredMedia">
						<title>Map to Haghios Sostis, Tegea, Arcadia, Greece</title>
						<handler>map</handler>
						<uri>...</uri>
					</media>
					<media type="ReferredMedia">
						<title>Map to Archaeological Museum of Tegea, Tegea, Arcadia, Greece</title>
						<handler>map</handler>
						<uri>...</uri>
					</media>
				</part>
				<part>
					<text>Persephone and pomegranate and relation to Eve.</text>
					<media type="ReferredMedia">
						<title>Adam and Eve painting</title>
						<handler>image</handler>
						<uri>...</uri>
					</media>
					<media type="ReferredMedia">
						<title>Persephone and Hades</title>
						<handler>image</handler>
						<uri>...</uri>
					</media>
				</part>
			</narration>
		</segment>
		<segment>
			<title>Cybele and lions</title>
			<duration>PT8M</duration>
			<exhibit>
				<id>...</id>
				<location>Room 7, 1st floor</location>
				<name>Museum Item: 5767</name>
			</exhibit>
			<movement>false</movement>
			<optional>false</optional>
			<narration>
				<part>
					<text>How to recognise Cybele through her animal, the lion.</text>
					<media type="ReferredMedia">
						<title>Cybele</title>
						<handler>web</handler>
						<uri>https://en.wikipedia.org/wiki/Cybele</uri>
					</media>
				</part>
			</narration>
		</segment>
		<segment>
			<title>Young girl and bird</title>
			<duration>PT5M</duration>
			<exhibit>
				<id>...</id>
				<location>Room 7, 1st floor</location>
				<name>Museum Item: 2967</name>
			</exhibit>
			<movement>false</movement>
			<optional>false</optional>
			<narration>
				<part>
					<text>How to recognize young girls and the how to predict the future by observing how birds fly, based on Homer’s descriptions.</text>
				</part>
			</narration>
		</segment>
		<segment>
			<title>Family tombstone</title>
			<duration>PT5M</duration>
			<exhibit>
				<id>...</id>
				<location>Room 7, 1st floor</location>
				<name>Museum Item: 2979</name>
			</exhibit>
			<movement>false</movement>
			<optional>false</optional>
			<narration>
				<part>
					<text>How to see who the dead person is, based on the name appearing on the tombstone.</text>
				</part>
			</narration>
		</segment>
		<segment>
			<title>Owl and Athena</title>
			<duration>PT5M</duration>
			<exhibit>
				<id>...</id>
				<location>Room 7, 1st floor</location>
				<name>Museum item: Owl</name>
			</exhibit>
			<movement>false</movement>
			<optional>false</optional>
			<narration>
				<part>
					<text>How is the owl connected to goddess Athena.</text>
					<media type="ReferredMedia">
						<title>Athenian coin, 4th c. BC</title>
						<handler>image</handler>
						<uri>...</uri>
					</media>
				</part>
			</narration>
		</segment>
		<segment>
			<title>Pig and sacrifices</title>
			<duration>PT7M</duration>
			<exhibit>
				<id>...</id>
				<location>Room 7, 1st floor</location>
				<name>Museum item: Clay Pig</name>
			</exhibit>
			<movement>false</movement>
			<optional>false</optional>
			<narration>
				<part>
					<text>Animal sacrifices in Ancient Greece</text>
					<media type="ReferredMedia">
						<title>Athenian coin, 4th c. BC</title>
						<handler>image</handler>
						<uri>...</uri>
					</media>
				</part>
				<part>
					<text>Modern days practices and animal rights</text>
					<media type="ReferredMedia">
						<title>Animal sacrifice in Ancient Greece</title>
						<handler>image</handler>
						<uri>...</uri>
					</media>
					<media type="ReferredMedia">
						<title>Bullfighting in Spain</title>
						<handler>image</handler>
						<uri>...</uri>
					</media>
					<media type="ReferredMedia">
						<title>Dog cosmetics and dressing</title>
						<handler>image</handler>
						<uri>...</uri>
					</media>
					<media type="ReferredMedia">
						<title>Ceremonial elephants in India</title>
						<handler>image</handler>
						<uri>...</uri>
					</media>
				</part>
			</narration>
		</segment>
		<segment>
			<title>Ram and Artemis</title>
			<duration>PT3M</duration>
			<optional>false</optional>
			<exhibit>
				<id>...</id>
				<location>Room 7, 1st floor</location>
				<name>Museum Item: Ram</name>
			</exhibit>
			<movement>false</movement>
			<narration>
				<part>Artemis the protector of wild-life and children.</part>
			</narration>
		</segment>
		<segment>
			<title>Monkey</title>
			<duration>PT3M</duration>
			<optional>false</optional>
			<exhibit>
				<id>...</id>
				<location>Room 7, 1st floor</location>
				<name>Museum Item: Monkey</name>
			</exhibit>
			<movement>false</movement>
			<narration>
				<part>
					<text>Monkeys were unusual in classical times but common in Minoan times.</text>
					<media type="ReferredMedia">
						<title>Minoan monkey painting</title>
						<handler>image</handler>
						<uri>...</uri>
					</media>
					<media type="ReferredMedia">
						<title>Akrotiri, Santorini, Greece</title>
						<handler>map</handler>
						<uri>...</uri>
					</media>
				</part>
			</narration>
		</segment>
		<segment>
			<title>Bull and Europe</title>
			<duration>PT5M</duration>
			<optional>false</optional>
			<exhibit>
				<id>...</id>
				<location>Room 7, 1st floor</location>
				<name>Museum Item: Bull</name>
			</exhibit>
			<movement>false</movement>
			<narration>
				<part>
					<text>Myth of Europe and Zeus.</text>
					<media type="ReferredMedia">
						<title>Greek 2 EUR coin</title>
						<handler>image</handler>
						<uri>...</uri>
					</media>
				</part>
			</narration>
		</segment>
		<segment>
			<title>Gortsuli figurines</title>
			<duration>PT5M</duration>
			<optional>false</optional>
			<exhibit>
				<id>...</id>
				<location>Room 7, 1st floor</location>
				<name>Museum Item: 4561</name>
			</exhibit>
			<movement>false</movement>
			<narration>
				<part>
					<text>The medicinal use of plants and the role of Artemis</text>
				</part>
			</narration>
		</segment>
		<segment>
			<title>Apollo and the Muses</title>
			<duration>PT10M</duration>
			<optional>false</optional>
			<exhibit>
				<id>...</id>
				<location>Room 7, 1st floor</location>
				<name>Museum Item: Apollo</name>
			</exhibit>
			<movement>false</movement>
			<narration>
				<part>
					<text>The connection of Apollo with dolphins</text>
				</part>
				<part>
					<text>About the reconstruction of Delphi sanctuary</text>	
					<media type="ReferredMedia">
						<title>Reconstruction of Delphi sanctuary</title>
						<handler>image</handler>
						<uri>...</uri>
					</media>
				</part>
				<part>
					<text>About Delphi sanctuary today</text>
					<media type="ReferredMedia">
						<title>Delphi sanctuary today</title>
						<handler>image</handler>
						<uri>...</uri>
					</media>
				</part>
			</narration>
		</segment>
		<segment>
			<title>Findings from Palaiokastro</title>
			<duration>PT3M</duration>
			<optional>false</optional>
			<exhibit>
				<id>...</id>
				<location>Room 7, 1st floor</location>
				<name>Museum Item: Palaiokastro</name>
			</exhibit>
			<movement>false</movement>
			<narration>
				<part>
					<text>Death oracle of Palaiokastro and Homer's description of it</text>
				</part>
			</narration>
		</segment>
		<segment>
			<title>Headless Statue of Athena</title>
			<duration>PT8M</duration>
			<optional>false</optional>
			<exhibit>
				<id>...</id>
				<location>Room 7, 1st floor</location>
				<name>Museum Item: 2505</name>
			</exhibit>
			<movement>false</movement>
			<narration>
				<part>
					<text>The myth about the naming of the city of Athens</text>
				</part>
				<part>
					<media type="ReferredMedia">
						<title>Arachne - Directed by Nick Kozis (Greek Mythology) </title>
						<handler>video</handler>
						<uri>https://www.youtube.com/watch?v=qW3Bbav7w4A</uri>
					</media>
				</part>
			</narration>
		</segment>
		<segment>
			<title>Hercules&apos; Tondo</title>
			<duration>PT5M</duration>
			<optional>false</optional>
			<constraints>
				<domain>
					<value>Museum Directives</value>
					<subdomain>
						<value>Item Availability</value>
						<constraint>
							<value>Not available</value>
						</constraint>
					</subdomain>
				</domain>
			</constraints>
			<exhibit>
				<id>...</id>
				<location>Room 7, 1st floor</location>
				<name>Hercules&apos; tondo image</name>
			</exhibit>
			<movement></movement>
			<narration>
				<part>
					<text>Item unavailable due to restoration purposes.</text>
				</part>
				<part>
					<text>The story of Hercules and Auge</text>
				</part>
			</narration>
		</segment>
		<segment>
			<title>Bull sacrifice</title>
			<duration>PT10M</duration>
			<optional>false</optional>
			<exhibit>
				<id>...</id>
				<location>Room 7, 1st floor</location>
				<name>Museum Item: Bull sacrifice</name>
			</exhibit>
			<movement>movement</movement>
			<narration>
				<part>
					<text>The myth of Pan and his flute.</text>
				</part>
				<part>
					<media type="ReferredMedia">
						<title>17th century AD, Austrian, Pan, Satyrs, Nymphs</title>
						<handler>image</handler>
						<uri>...</uri>
					</media>
				</part>
			</narration>
		</segment>
		<segment>
			<title>Move to Palaiokastro dedicated room</title>
			<duration>PT1M</duration>
			<movement>true</movement>
			<optional>false</optional>
		</segment>
		<segment>
			<title>Palaiokastro death oracle</title>
			<duration>PT10M</duration>
			<exhibit>
				<id>...</id>
				<location>Palaiokastro room</location>
				<name>Palaiokastro room</name>
			</exhibit>
			<movement>true</movement>
			<optional>false</optional>
			<narration>
				<part>
					<text>How Odysseus asked the souls of the dead for advice</text>
				</part>
			</narration>
		</segment>
		<segment>
			<title>Slave</title>
			<duration>PT4M</duration>
			<exhibit>
				<id>...</id>
				<location></location>
				<name>Museum Item: Slave tombstone</name>
			</exhibit>
			<movement>false</movement>
			<optional>false</optional>
			<narration>
				<part>
					<text>The tombstone of a woman and her slave, and its meaning</text>
				</part>
			</narration>
		</segment>
	</trail>
</narrative>